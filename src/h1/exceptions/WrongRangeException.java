package h1.exceptions;

public class WrongRangeException extends RuntimeException {

    public WrongRangeException() {
        super("wrong range");
    }
}
