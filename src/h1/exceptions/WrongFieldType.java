package h1.exceptions;

public class WrongFieldType extends RuntimeException {
    public WrongFieldType() {
        super("Wrong field type");
    }
}
