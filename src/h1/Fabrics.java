package h1;

import h1.annotations.GenerateRandom;
import h1.annotations.GenerateUnit;
import h1.annotations.ManyUnits;
import h1.annotations.RunSetter;
import h1.exceptions.NotUnitException;
import h1.handlers.GenerateUnitHandler;
import h1.handlers.RandomNumberGenerator;
import h1.handlers.RunnerSetters;
import h1.unit.Unit;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Fabrics {

    public Fabrics(RunnerSetters runnerSetters, RandomNumberGenerator generator, GenerateUnitHandler generateUnitHandler) {
        this.runnerSetters = runnerSetters;
        this.generator = generator;
        this.generateUnitHandler = generateUnitHandler;
    }

    RunnerSetters runnerSetters;
    RandomNumberGenerator generator;
    GenerateUnitHandler generateUnitHandler;

    public  Unit createUnitByName(String unitClass) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        Class cls =  Class.forName(unitClass);
        if (!Unit.class.isAssignableFrom(cls))
            throw new NotUnitException();
       Unit unit = (Unit) cls.newInstance();
       //один метод process
        process(unit);
        return unit;
    }

    private void process(Unit unit) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Class cls = unit.getClass();
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(GenerateRandom.class)) {
                field.setAccessible(true);
                generator.handle(field, unit);
            }
            if (field.isAnnotationPresent(RunSetter.class)) {
                field.setAccessible(true);
                runnerSetters.handle(field.getAnnotation(RunSetter.class), unit, field);
            }

            if (field.isAnnotationPresent(GenerateUnit.class)) {
                field.setAccessible(true);
                generateUnitHandler.generateUnit(field, unit, this);
            }

            if (field.isAnnotationPresent(ManyUnits.class)) {
                field.setAccessible(true);
                field.set(unit, this.getUnits(field.getAnnotation(ManyUnits.class).count(), field.getAnnotation(ManyUnits.class).className()));
            }
        }
    }


    List<Unit> getUnits(int count, String unitClass) throws ClassNotFoundException, InvocationTargetException, InstantiationException, NoSuchMethodException, IllegalAccessException {
        Fabrics fabrics = this;
        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            units.add(fabrics.createUnitByName(unitClass));
        }
        return units;
    }
}
