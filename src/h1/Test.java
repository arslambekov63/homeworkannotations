package h1;

import h1.handlers.GenerateUnitHandler;
import h1.handlers.RandomNumberGenerator;
import h1.handlers.RunnerSetters;
import h1.unit.Archer;
import h1.unit.Unit;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Test {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, NoSuchMethodException, IllegalAccessException {
        Fabrics fabrics = new Fabrics(new RunnerSetters(), new RandomNumberGenerator(), new GenerateUnitHandler());
      Unit unit = fabrics.createUnitByName("h1.unit.Archer");
      Archer archer = (Archer) unit;
        System.out.println("Проверка аннотации GenerateRandom");
        System.out.println(archer.getHp());
        System.out.println("Проверка аннотации RunSetter");
        System.out.println(archer.getName());
        System.out.println("Проверка аннотации GenerateUnit");
        archer.getUnit().saySomething();


        System.out.println("Проверка аннотации ManyUnits");
        List<Unit> units = archer.getUnits();
        for (Unit unit1: units) {
            unit1.saySomething();
        }

    }
}
