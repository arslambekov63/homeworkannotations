package h1.unit;

public enum WeaponType {
    BOW,
    SWORD,
    SHIELD;
}
