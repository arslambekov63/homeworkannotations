package h1.unit;

import h1.annotations.GenerateRandom;
import h1.annotations.GenerateUnit;
import h1.annotations.ManyUnits;
import h1.annotations.RunSetter;

import java.util.List;

public class Archer extends Unit {
    public void saySomething() {
        System.out.println("Arrrcher");
    }
    WeaponType weaponType = WeaponType.BOW;
    @GenerateRandom(minValue = 4, maxValue = 45)
    private int hp;
    @RunSetter( method = "createName")
    private String name;


    @GenerateRandom
    private int speed;

    public int getHp() {
        return hp;
    }
    @GenerateUnit
    Knight unit;

    @ManyUnits(count = 3, className = "h1.unit.CoolKnight")
    List<Unit> units;

    public Knight getUnit() {
        return unit;
    }

    public void setUnit(Knight unit) {
        this.unit = unit;
    }

    public void createName() {
        this.name = "Victory";
    }


    public String getName() {
        return name;
    }

    public List<Unit> getUnits() {
        return units;
    }
}
