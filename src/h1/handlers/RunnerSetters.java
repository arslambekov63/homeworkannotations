package h1.handlers;

import h1.annotations.RunSetter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RunnerSetters {

   public void  handle(RunSetter runSetter, Object obj, Field field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
       Method method = obj.getClass().getDeclaredMethod(runSetter.method());
       method.invoke(obj);
       /*
       switch (field.getType().getName()) {
           case ("int"):
           case ("Integer"):
               method.invoke(obj, Integer.parseInt(runSetter.value()));
               break;
           case ("double"):
           case ("Double"):
               method.invoke(obj, Double.parseDouble(runSetter.value()));
               break;
           case ("Float"):
           case ("float"):
               method.invoke(obj, Float.parseFloat(runSetter.value()));
               break;

           case ("char"):
           case ("Character"):
               method.invoke(obj, runSetter.value().charAt(0));
               break;
           case ("byte"):
           case ("Byte"):
               method.invoke(obj, Byte.parseByte(runSetter.value()));
               break;
           case ("Short"):
               case ("short"):
                   method.invoke(obj, Short.parseShort(runSetter.value()));
                   break;
           case ("String"):
               method.invoke(obj, runSetter.value());
               break;
           default:
               throw new WrongMethodTypeException("Bad type for setter");
       }

        */

   }

    private String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return ""; //или return word;
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}
