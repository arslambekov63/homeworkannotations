package h1.handlers;

import h1.annotations.GenerateRandom;
import h1.exceptions.WrongFieldType;

import java.lang.reflect.Field;

public class RandomNumberGenerator {




    public void handle(Field field, Object obj) throws IllegalAccessException {


            if (field.isAnnotationPresent(GenerateRandom.class)) {
                generateForField(field, field.getAnnotation(GenerateRandom.class), obj);
            }
        }

    public void generateForField(Field field, GenerateRandom generateRandom, Object obj) throws IllegalAccessException {
        String typeName = field.getType().getTypeName();
        field.setAccessible(true);
        double digit = generateRandom.minValue() + Math.random() * generateRandom.maxValue();
        switch (typeName) {
            case ("int"):
            case ("Integer") :
                field.setInt(obj, (int) digit);
                break;
            case ("long"):
            case ("Long"):
                field.setLong(obj, (long) digit);
                break;
            case ("double"):
            case ("Double"):
                field.setDouble(obj, digit);
                break;
            case ("float"):
            case("Float"):
                field.setFloat(obj, (float) digit);
                break;
            default:
                throw new WrongFieldType();
        }
    }
}
