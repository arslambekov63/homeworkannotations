package h1.handlers;

import h1.Fabrics;
import h1.unit.Unit;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class GenerateUnitHandler {

    public void generateUnit(Field field, Unit unit, Fabrics fabrics) throws ClassNotFoundException, InvocationTargetException, InstantiationException, NoSuchMethodException, IllegalAccessException {
        field.setAccessible(true);
        field.set(unit, fabrics.createUnitByName(field.getType().getName()));
    }
}
